# Ansible Role: krew

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-krew/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-krew/-/commits/main)

This role installs [krew](https://github.com/kubernetes-sigs/krew/) binary on any supported host.

## NOTE

This role works with ansible==4.10.0. If you're using ansible > 4.10.0 you probably get a bug and the unarchive tasks won't work. To install ansible 4.10.0 you will try this:

```
sudo pip3 install ansible==4.10.0
```

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):
    
    krew_version: latest # v0.4.2 if you want a specific version
    setup_dir: /tmp
    krew_arch: amd64 # amd64, arm, arm64
    krew_bin_path: /usr/local/bin
    krew_repo_path: https://github.com/kubernetes-sigs/krew/releases/download

This role can install the latest or a specific version. See [available krew releases](https://github.com/bitnami-labs/sealed-secrets) and change this variable accordingly.

    krew_version: latest # v0.4.2 if you want a specific version

The path of the krew repository.

    krew_repo_path: https://github.com/kubernetes-sigs/krew/releases/download

The path to the home krew directory.

    krew_bin_path: /usr/local/bin

krew supports amd64, arm and arm64 CPU architectures, just change for the main architecture of your CPU.

    krew_arch: amd64 # amd64, arm, arm64

Krew needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install Krew. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: krew

## License

MIT / BSD
